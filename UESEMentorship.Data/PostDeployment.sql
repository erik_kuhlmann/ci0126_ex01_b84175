﻿DELETE FROM AreaMentoria
DELETE FROM EnMentoria
DELETE FROM Estudiante
DELETE FROM Docente

DBCC CHECKIDENT('AreaMentoria', RESEED, 0)

INSERT INTO AreaMentoria
VALUES
('Posgrado exterior', 'Oportunidades de hacer un posgrado fuera del país!'),
('Pasantía', 'Trabajarás como pasante en las mejores compañías del mundo!')

INSERT INTO Estudiante
VALUES
('118080272', 'B84175', 'Erik Kühlmann', 'Computación', 'Ciencias de la computación'),
('118080273', 'B87561', 'Adrián Sibaja', 'Computación', 'Ciencias de la computación'),
('118080274', 'B87562', 'Esteban Marín', 'Computación', 'Ciencias de la computación'),
('118080275', 'B87563', 'Ricardo Franco', 'Computación', 'Ingeniería de software'),
('118080276', 'B87564', 'Daniel Salazar', 'Computación', 'Ingeniería de software')

INSERT INTO Docente
VALUES
('123412341', 'Ricardo Villalón', 'Doctorado', 'Computación'),
('123412342', 'José Guevara', 'Doctorado', 'Bioinformática'),
('123412343', 'Edgar Casasola', 'Doctorado', 'Computación'),
('123412344', 'Cristian Quesada', 'Doctorado', 'Computación'),
('123412345', 'Alexandra Martínez', 'Doctorado', 'Computación')
