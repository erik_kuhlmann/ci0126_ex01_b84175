﻿CREATE TABLE [dbo].[AreaMentoria]
(
	[Id] INT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
	[Nombre] NVARCHAR(20) NOT NULL,
	[Descripcion] NVARCHAR(500) NOT NULL
)
