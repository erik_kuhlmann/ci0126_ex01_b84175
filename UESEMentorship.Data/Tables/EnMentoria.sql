﻿CREATE TABLE [dbo].[EnMentoria]
(
	[DocenteCedula] NVARCHAR(9) NOT NULL,
	[EstudianteCedula] NVARCHAR(9) NOT NULL,
	[AreaMentoriaId] INT NOT NULL,
	PRIMARY KEY (EstudianteCedula, DocenteCedula, AreaMentoriaId),
	FOREIGN KEY (EstudianteCedula)
		REFERENCES Estudiante(Cedula),
	FOREIGN KEY (DocenteCedula)
		REFERENCES Docente(Cedula),
	FOREIGN KEY (AreaMentoriaId)
		REFERENCES AreaMentoria(Id)
)
