﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure.EntityConfiguration
{
    public class EstudianteMap : IEntityTypeConfiguration<Estudiante>
    {
        public void Configure(EntityTypeBuilder<Estudiante> builder)
        {
            builder.ToTable(nameof(Estudiante));
            builder.HasKey(e => e.Cedula);
        }
    }
}