﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure.EntityConfiguration
{
    public class EnMentoriaMap : IEntityTypeConfiguration<EnMentoria>
    {
        public void Configure(EntityTypeBuilder<EnMentoria> builder)
        {
            builder.ToTable(nameof(EnMentoria));
            builder.HasKey(em => new {em.AreaMentoriaId, em.DocenteCedula, em.EstudianteCedula});

            builder
                .HasOne(em => em.Docente)
                .WithMany(d => d.Mentorias)
                .HasForeignKey(em => em.DocenteCedula);

            builder
                .HasOne(em => em.Estudiante)
                .WithMany(e => e.Mentorias)
                .HasForeignKey(em => em.EstudianteCedula);

            builder
                .HasOne(em => em.AreaMentoria)
                .WithMany()
                .HasForeignKey(em => em.AreaMentoriaId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}