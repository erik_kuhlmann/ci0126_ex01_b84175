﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure.EntityConfiguration
{
    public class DocenteMap : IEntityTypeConfiguration<Docente>
    {
        public void Configure(EntityTypeBuilder<Docente> builder)
        {
            builder.ToTable(nameof(Docente));
            builder.HasKey(d => d.Cedula);
        }
    }
}