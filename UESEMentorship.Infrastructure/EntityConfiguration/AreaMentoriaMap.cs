﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure.EntityConfiguration
{
    public class AreaMentoriaMap : IEntityTypeConfiguration<AreaMentoria>
    {
        public void Configure(EntityTypeBuilder<AreaMentoria> builder)
        {
            builder.ToTable(nameof(AreaMentoria));
            builder.HasKey(am => am.Id);
        }
    }
}