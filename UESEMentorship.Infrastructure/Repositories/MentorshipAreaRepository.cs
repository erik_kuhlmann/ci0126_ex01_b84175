﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UESEMentorship.Application.Repositories;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure.Repositories
{
    public class MentorshipAreaRepository : EntityRepository<AreaMentoria>,
        IMentorshipAreaRepository
    {
        public MentorshipAreaRepository(ISqlDataProvider db) : base(db)
        {
        }

        public bool IsMentorshipAreaDeletableAsync(int mentorshipId)
        {
            var existing = Db
                .AssignedMentorships
                .FirstOrDefault(m => m.AreaMentoriaId == mentorshipId);
            
            return existing == null;
        }
    }
}