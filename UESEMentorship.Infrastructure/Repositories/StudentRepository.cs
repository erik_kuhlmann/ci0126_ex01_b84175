﻿using UESEMentorship.Application.Repositories;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure.Repositories
{
    public class StudentRepository : ReadOnlyRepository<Estudiante>,
        IStudentRepository
    {
        public StudentRepository(ISqlDataProvider db) : base(db)
        {
        }
    }
}