﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UESEMentorship.Application.Repositories;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure.Repositories
{
    public class AssignedMentorshipRepository : EntityRepository<EnMentoria>,
        IAssignedMentorshipRepository
    {
        public AssignedMentorshipRepository(ISqlDataProvider db) : base(db)
        {
        }

        public override async Task<IEnumerable<EnMentoria>> GetAllAsync()
        {
            return await Db.AssignedMentorships
                .Include(am => am.Docente)
                .Include(am => am.Estudiante)
                .Include(am => am.AreaMentoria)
                .ToListAsync();
        }

        public int TeacherAssignedMentorshipsCount(string teacherId)
        {
            return Db.AssignedMentorships
                .Count(m => m.DocenteCedula == teacherId);
        }

        public int StudentAssignedMentorshipsByAreaCount(string studentId, int areaId)
        {
            return Db.AssignedMentorships
                .Count(m => m.EstudianteCedula == studentId && m.AreaMentoriaId == areaId);
        }

        public EnMentoria GetWithRelatedEntities(int mentorshipId, string teacherId, string studentId)
        {
            return Db.AssignedMentorships
                .Include(am => am.Docente)
                .Include(am => am.Estudiante)
                .Include(am => am.AreaMentoria)
                .FirstOrDefault(am =>
                    am.AreaMentoriaId == mentorshipId
                    && am.DocenteCedula == teacherId
                    && am.EstudianteCedula == studentId
                );
        }
    }
}