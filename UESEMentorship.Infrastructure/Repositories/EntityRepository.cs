﻿using System.Collections.Generic;
using UESEMentorship.Application.Repositories;

namespace UESEMentorship.Infrastructure.Repositories
{
    public class EntityRepository<TEntity> : ReadOnlyRepository<TEntity>,
        IEntityRepository<TEntity>
        where TEntity : class
    {
        protected EntityRepository(ISqlDataProvider db) : base(db)
        {
        }
        
        public virtual TEntity Insert(TEntity entity)
        {
            Db.Set<TEntity>().Add(entity);
            Db.Save();
            return entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            Db.Set<TEntity>().Update(entity);
            Db.Save();
            return entity;
        }

        public void Delete(params object[] keyValues)
        {
            var entity = GetByKey(keyValues);
            Db.Set<TEntity>().Remove(entity);
            Db.Save();
        }
    }
}