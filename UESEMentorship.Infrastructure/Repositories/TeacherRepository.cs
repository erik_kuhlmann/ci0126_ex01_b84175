﻿using UESEMentorship.Application.Repositories;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure.Repositories
{
    public class TeacherRepository :  ReadOnlyRepository<Docente>,
        ITeacherRepository
    {
        public TeacherRepository(ISqlDataProvider db) : base(db)
        {
        }
    }
}