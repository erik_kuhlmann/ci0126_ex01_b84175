﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UESEMentorship.Application.Repositories;

namespace UESEMentorship.Infrastructure.Repositories
{
    public class ReadOnlyRepository<TEntity> : IReadOnlyRepository<TEntity>
        where TEntity : class
    {
        protected readonly ISqlDataProvider Db;

        protected ReadOnlyRepository(ISqlDataProvider db)
        {
            Db = db;
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await Db.Set<TEntity>().ToListAsync();
        }

        public virtual TEntity GetByKey(params object[] keyValues)
        {
            return Db.Set<TEntity>().Find(keyValues);
        }
    }
}