﻿using Microsoft.EntityFrameworkCore;
using UESEMentorship.Domain.Entities;
using UESEMentorship.Infrastructure.EntityConfiguration;

namespace UESEMentorship.Infrastructure
{
    public class ApplicationDbContext : DbContext, ISqlDataProvider
    {
        public DbSet<Estudiante> Students { get; set; }
        public DbSet<Docente> Teachers { get; set; }
        public DbSet<AreaMentoria> MentorshipAreas { get; set; }
        public DbSet<EnMentoria> AssignedMentorships { get; set; }
        
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        
        public void Save()
        {
            SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DocenteMap());
            modelBuilder.ApplyConfiguration(new EstudianteMap());
            modelBuilder.ApplyConfiguration(new AreaMentoriaMap());
            modelBuilder.ApplyConfiguration(new EnMentoriaMap());
        }
    }
}