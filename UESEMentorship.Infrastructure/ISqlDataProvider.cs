﻿using Microsoft.EntityFrameworkCore;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Infrastructure
{
    public interface ISqlDataProvider
    {
        public DbSet<Estudiante> Students { get; set; }
        public DbSet<Docente> Teachers { get; set; }
        public DbSet<AreaMentoria> MentorshipAreas { get; set; }
        public DbSet<EnMentoria> AssignedMentorships { get; set; }
        DbSet<T> Set<T>() where T : class;
        void Save();
    }
}