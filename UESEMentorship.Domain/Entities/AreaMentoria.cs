﻿using System.ComponentModel.DataAnnotations;

namespace UESEMentorship.Domain.Entities
{
    public class AreaMentoria
    {
        public int Id { get; set; } 
        [Required(ErrorMessage = "Debe digitar un nombre.")]
        [MaxLength(20, ErrorMessage = "El nombre no debe exceder los {1} caracteres.")]
        public string Nombre { get; set; } 
        [Required(ErrorMessage = "Debe digitar una descripción.")]
        [MaxLength(500, ErrorMessage = "El nombre no debe exceder los {1} caracteres.")]
        public string Descripcion { get; set; } 
    }
}