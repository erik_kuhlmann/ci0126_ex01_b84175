﻿using System.Collections.Generic;

namespace UESEMentorship.Domain.Entities
{
    public class Estudiante
    {
        public string Cedula { get; set; }
        public string Carne { get; set; }
        public string Nombre { get; set; }
        public string Carrera { get; set; }
        public string Enfasis { get; set; }
        public List<EnMentoria> Mentorias { get; set; }
    }
}