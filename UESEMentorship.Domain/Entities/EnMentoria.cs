﻿namespace UESEMentorship.Domain.Entities
{
    public class EnMentoria
    {
        public string DocenteCedula { get; set; }
        public Docente Docente { get; set; }
        
        public string EstudianteCedula { get; set; }
        public Estudiante Estudiante { get; set; }

        public int AreaMentoriaId { get; set; }
        public AreaMentoria AreaMentoria { get; set; }
    }
}