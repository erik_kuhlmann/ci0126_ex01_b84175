﻿using System.Collections.Generic;

namespace UESEMentorship.Domain.Entities
{
    public class Docente
    {
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string GradoAcademico { get; set; }
        public string Carrera { get; set; }
        public List<EnMentoria> Mentorias { get; set; }
    }
}