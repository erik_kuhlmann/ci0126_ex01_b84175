﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using UESEMentorship.Application.Implementations;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.UI.Pages.AssignedMentorships
{
    public partial class List
    {
         
        [Inject]
        private IAssignedMentorshipService AssignedMentorshipService { get; set; } 
        
        private List<EnMentoria> _mentorships;

        protected override async Task OnInitializedAsync()
        {
            _mentorships = (await AssignedMentorshipService.GetAllAsync()).ToList();
        }
    }
}