﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using UESEMentorship.Application.Exceptions;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.UI.Pages.AssignedMentorships
{
    public partial class Create
    {
        [Inject]
        private IAssignedMentorshipService AssignedMentorshipService { get; set; }
        
        [Inject]
        private NavigationManager NavigationManager { get; set; }

        private bool IsLoading => _students == null || _teachers == null || _mentorshipAreas == null;


        private List<Estudiante> _students;
        private List<Docente> _teachers;
        private List<AreaMentoria> _mentorshipAreas;
        private readonly EnMentoria _model = new EnMentoria();
        private string _errorMessage = "";

        private string SelectedMentorshipArea
        {
            get => _model.AreaMentoriaId.ToString();

            set => _model.AreaMentoriaId = Int32.Parse(value);
        }

        private void ClearErrorMessage()
        {
            _errorMessage = "";
        }

        private void Submit()
        {
            try
            {
                AssignedMentorshipService.Create(_model);
                NavigationManager.NavigateTo("assigned-mentorships");
            }
            catch (BusinessRuleException e)
            {
                _errorMessage = e.Message;
            } 
        }

        protected override async Task OnInitializedAsync()
        {
            _students = (await AssignedMentorshipService.GetStudentsListAsync()).ToList();
            _teachers = (await AssignedMentorshipService.GetTeachersListAsync()).ToList();
            _mentorshipAreas = (await AssignedMentorshipService.GetMentorshipAreasListAsync()).ToList();

            _model.DocenteCedula = _teachers.First().Cedula;
            _model.EstudianteCedula = _students.First().Cedula;
            _model.AreaMentoriaId = _mentorshipAreas.First().Id;
        }
    }
}