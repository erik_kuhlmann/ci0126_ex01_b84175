﻿using Microsoft.AspNetCore.Components;
using UESEMentorship.Application.Repositories;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.UI.Pages.AssignedMentorships
{
    public partial class Delete
    {
        [Inject]
        private IAssignedMentorshipService AssignedMentorshipService { get; set; }
        
        [Inject]
        private NavigationManager NavigationManager { get; set; }
        
        [Parameter]
        public int MentorshipAreaId { get; set; }
        
        [Parameter]
        public string TeacherId { get; set; }
        
        [Parameter]
        public string StudentId { get; set; }

        private EnMentoria _model;

        private void DeleteModel()
        {
            AssignedMentorshipService.Delete(
                _model.AreaMentoriaId,
                _model.DocenteCedula,
                _model.EstudianteCedula);
            Redirect();
        }

        private void Redirect()
        {
            NavigationManager.NavigateTo("assigned-mentorships");     
        }

        protected override void OnInitialized()
        {
            _model = AssignedMentorshipService.GetByKey(MentorshipAreaId, TeacherId, StudentId);
        }
    }
}