﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.VisualBasic.FileIO;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.UI.Pages.MentorshipAreas
{
    public partial class List
    {
        [Inject]
        private IMentorshipAreaService MentorshipAreaService { get; set; } 
        
        private List<AreaMentoria> _mentorships;

        protected override async Task OnInitializedAsync()
        {
            _mentorships = (await MentorshipAreaService.GetAllAsync()).ToList();
        }
    }
}