﻿using Microsoft.AspNetCore.Components;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.UI.Pages.MentorshipAreas
{
    public partial class Edit
    {
         
        [Inject]
        public IMentorshipAreaService MentorshipAreaService { get; set; }
        
        [Inject]
        public NavigationManager NavigationManager { get; set; }
        
        [Parameter]
        public int Id { get; set; }
        
        private AreaMentoria _model;
        
        private void Submit()
        {
            MentorshipAreaService.Update(_model);
            NavigationManager.NavigateTo("mentorship-areas");
        }

        protected override void OnInitialized()
        {
            _model = MentorshipAreaService.GetById(Id);
        }
    }
}