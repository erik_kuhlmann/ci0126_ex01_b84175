﻿using Microsoft.AspNetCore.Components;
using UESEMentorship.Application.Exceptions;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.UI.Pages.MentorshipAreas
{
    public partial class Delete
    {
        [Inject]
        private IMentorshipAreaService MentorshipAreaService { get; set; }
        
        [Inject]
        private NavigationManager NavigationManager { get; set; }
        
        [Parameter]
        public int Id { get; set; }
        
        private AreaMentoria _model;
        private string _errorMessage = "";

        private void DeleteModel()
        {
            try
            {
                MentorshipAreaService.Delete(_model.Id);
                Redirect();
            }
            catch (BusinessRuleException e)
            {
                _errorMessage = e.Message;
            }
            
        }

        private void ClearErrorMessage()
        {
            _errorMessage = "";
        }

        private void Redirect()
        {
            NavigationManager.NavigateTo("mentorship-areas");     
        }

        protected override void OnInitialized()
        {
            _model = MentorshipAreaService.GetById(Id);
        }
    }
}