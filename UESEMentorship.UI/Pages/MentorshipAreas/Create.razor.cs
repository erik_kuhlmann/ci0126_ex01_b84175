﻿using Microsoft.AspNetCore.Components;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.UI.Pages.MentorshipAreas
{
    public partial class Create
    {
        [Inject] 
        public IMentorshipAreaService MentorshipAreaService { get; set; }
        
        [Inject] 
        public NavigationManager NavigationManager { get; set; }
        
        private readonly AreaMentoria _model = new AreaMentoria();
        
        private void Submit()
        {
            MentorshipAreaService.Create(_model);
            NavigationManager.NavigateTo("mentorship-areas");
        }
    }
}