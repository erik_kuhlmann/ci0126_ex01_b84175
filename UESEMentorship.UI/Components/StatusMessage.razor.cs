﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace UESEMentorship.UI.Components
{
    public partial class StatusMessage
    {
        [Parameter]
        public string Message { get; set; }
        
        [Parameter]
        public string Color { get; set; }
        
        [Parameter]
        public EventCallback OnClear { get; set; }
        
        [Parameter]
        public bool IsLight { get; set; }

        private string CssClass
        {
            get
            {
                var result = "is-" + Color;
                if (IsLight)
                    result += " is-light";

                return result;
            }
        }

        private async Task ClearErrorMessage()
        {
            await OnClear.InvokeAsync();
        }
    }
}