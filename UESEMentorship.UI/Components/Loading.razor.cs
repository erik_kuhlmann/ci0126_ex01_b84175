﻿using Microsoft.AspNetCore.Components;

namespace UESEMentorship.UI.Components
{
    public partial class Loading
    {
        [Parameter] public bool IsLoading { get; set; } 
        
        [Parameter] public RenderFragment ChildContent { get; set; }
    }
}