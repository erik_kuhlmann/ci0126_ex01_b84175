﻿namespace UESEMentorship.UI.Components
{
    public partial class NavBar
    {
        private bool _navbarCollapsed = true;
        private string NavbarMenuClass => _navbarCollapsed ? "" : "is-active";

        private void ToggleNavBar()
        {
            _navbarCollapsed = !_navbarCollapsed;
        }
    }
}