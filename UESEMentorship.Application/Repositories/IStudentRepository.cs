﻿using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Application.Repositories
{
    public interface IStudentRepository : IReadOnlyRepository<Estudiante>
    {
    }
}