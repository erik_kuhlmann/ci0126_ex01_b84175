﻿using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Application.Repositories
{
    public interface ITeacherRepository : IReadOnlyRepository<Docente>
    {
    }
}