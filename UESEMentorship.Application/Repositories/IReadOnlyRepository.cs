﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace UESEMentorship.Application.Repositories
{
    public interface IReadOnlyRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAllAsync();
        TEntity GetByKey(params object[] keyValues);
    }
}