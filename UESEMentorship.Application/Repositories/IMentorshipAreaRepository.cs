﻿using System.Threading.Tasks;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Application.Repositories
{
    public interface IMentorshipAreaRepository : IEntityRepository<AreaMentoria>
    {
        bool IsMentorshipAreaDeletableAsync(int mentorshipId);
    }
}