﻿using System.Collections.Generic;

namespace UESEMentorship.Application.Repositories
{
    public interface IEntityRepository<TEntity> : IReadOnlyRepository<TEntity>
        where TEntity : class
    {
        TEntity Insert(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(params object[] keyValues);
    }
}