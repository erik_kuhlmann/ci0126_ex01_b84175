﻿using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Application.Repositories
{
    public interface IAssignedMentorshipRepository : IEntityRepository<EnMentoria>
    {
        int TeacherAssignedMentorshipsCount(string teacherId);
        int StudentAssignedMentorshipsByAreaCount(string studentId, int areaId);
        EnMentoria GetWithRelatedEntities(int mentorshipId, string teacherId, string studentId);
    }
}