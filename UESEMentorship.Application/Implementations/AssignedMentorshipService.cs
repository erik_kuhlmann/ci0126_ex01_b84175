﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UESEMentorship.Application.Exceptions;
using UESEMentorship.Application.Repositories;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Application.Implementations
{
    public class AssignedMentorshipService : IAssignedMentorshipService
    {
        private const int TeacherMaximumAssignedMentorships = 20;
        private const int StudentMaximumAssignedMentorshipsPerArea = 3;

        private readonly IAssignedMentorshipRepository _repo;
        private readonly ITeacherRepository _teacherRepo;
        private readonly IStudentRepository _studentRepo;
        private readonly IMentorshipAreaRepository _mentorshipAreaRepo;
        
        public AssignedMentorshipService(
            IAssignedMentorshipRepository repo,
            ITeacherRepository teacherRepo,
            IStudentRepository studentRepo,
            IMentorshipAreaRepository mentorshipAreaRepo)
        {
            _repo = repo;
            _teacherRepo = teacherRepo;
            _studentRepo = studentRepo;
            _mentorshipAreaRepo = mentorshipAreaRepo;
        }

        public async Task<IEnumerable<Docente>> GetTeachersListAsync()
        {
            return await _teacherRepo.GetAllAsync();
        }

        public async Task<IEnumerable<Estudiante>> GetStudentsListAsync()
        {
            return await _studentRepo.GetAllAsync();
        }

        public async Task<IEnumerable<AreaMentoria>> GetMentorshipAreasListAsync()
        {
            return await _mentorshipAreaRepo.GetAllAsync();
        }

        public EnMentoria GetByKey(int mentorshipAreaId, string teacherId, string studentId)
        {
            return _repo.GetWithRelatedEntities(mentorshipAreaId, teacherId, studentId);
        }

        public async Task<IEnumerable<EnMentoria>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }

        public EnMentoria Create(EnMentoria entity)
        {
            var existing = _repo.GetWithRelatedEntities(
                entity.AreaMentoriaId, 
        entity.DocenteCedula,
        entity.EstudianteCedula);
            
            if (existing != null)
            {
                throw new MentorshipAlreadyAssignedException();
            }

            var studentCount = _repo.StudentAssignedMentorshipsByAreaCount(
                entity.EstudianteCedula,
                entity.AreaMentoriaId);
            if (studentCount >= StudentMaximumAssignedMentorshipsPerArea)
            {
                throw new StudentMentorshipLimitException();
            }
            
            var teacherCount = _repo.TeacherAssignedMentorshipsCount(entity.DocenteCedula);
            if (teacherCount >= TeacherMaximumAssignedMentorships)
            {
                throw new TeacherMentorshipLimitException();
            }
            
            return _repo.Insert(entity);
        }

        public void Delete(int mentorshipAreaId, string teacherId, string studentId)
        {
            _repo.Delete(mentorshipAreaId, teacherId, studentId);
        }
    }
}