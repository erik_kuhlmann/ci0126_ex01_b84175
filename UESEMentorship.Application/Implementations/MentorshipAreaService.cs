﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UESEMentorship.Application.Exceptions;
using UESEMentorship.Application.Repositories;
using UESEMentorship.Application.Services;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Application.Implementations
{
    public class MentorshipAreaService : IMentorshipAreaService
    {
        private readonly IMentorshipAreaRepository _repo;

        public MentorshipAreaService(IMentorshipAreaRepository repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<AreaMentoria>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }

        public AreaMentoria Create(AreaMentoria entity)
        {
            return _repo.Insert(entity);
        }

        public void Update(AreaMentoria entity)
        {
            _repo.Update(entity);
        }

        public void Delete(int id)
        {
            if (!_repo.IsMentorshipAreaDeletableAsync(id))
            {
                throw new MentorshipAreaNotDeletableException();
            }
            
            _repo.Delete(id);
        }

        public AreaMentoria GetById(int id)
        {
            return _repo.GetByKey(id);
        }
    }
}