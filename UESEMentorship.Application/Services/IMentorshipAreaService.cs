﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UESEMentorship.Application.Repositories;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Application.Services
{
    public interface IMentorshipAreaService
    {
        AreaMentoria GetById(int id);
        Task<IEnumerable<AreaMentoria>> GetAllAsync();
        AreaMentoria Create(AreaMentoria entity);
        void Update(AreaMentoria entity);
        void Delete(int id);
    }
}