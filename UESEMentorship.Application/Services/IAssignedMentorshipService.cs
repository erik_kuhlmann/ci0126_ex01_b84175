﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UESEMentorship.Domain.Entities;

namespace UESEMentorship.Application.Services
{
    public interface IAssignedMentorshipService
    {
        Task<IEnumerable<Docente>> GetTeachersListAsync();
        Task<IEnumerable<Estudiante>> GetStudentsListAsync();
        Task<IEnumerable<AreaMentoria>> GetMentorshipAreasListAsync();
        Task<IEnumerable<EnMentoria>> GetAllAsync();
        EnMentoria GetByKey(int mentorshipAreaId, string teacherId, string studentId);
        EnMentoria Create(EnMentoria entity);
        void Delete(int mentorshipAreaId, string teacherId, string studentId);
    }
}