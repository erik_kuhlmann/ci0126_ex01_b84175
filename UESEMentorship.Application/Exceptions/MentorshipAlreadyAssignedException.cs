﻿using System;

namespace UESEMentorship.Application.Exceptions
{
    public class MentorshipAlreadyAssignedException : BusinessRuleException
    {
        public MentorshipAlreadyAssignedException() :
            base("Esta mentoría ya fue asignada.")
        {
        }
    }
}