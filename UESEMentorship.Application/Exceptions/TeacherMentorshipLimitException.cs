﻿using System;

namespace UESEMentorship.Application.Exceptions
{
    public class TeacherMentorshipLimitException : BusinessRuleException
    {
        public TeacherMentorshipLimitException() :
            base("El/la docente llegó al máximo de mentorías asigandas permitido.")
        {
        }
    }
}