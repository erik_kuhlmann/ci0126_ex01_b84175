﻿using System;

namespace UESEMentorship.Application.Exceptions
{
    public abstract class BusinessRuleException : Exception
    {
        protected BusinessRuleException(string message) : base(message)
        {
        }
    }
}