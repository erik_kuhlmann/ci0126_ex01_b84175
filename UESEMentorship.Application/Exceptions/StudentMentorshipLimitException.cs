﻿namespace UESEMentorship.Application.Exceptions
{
    public class StudentMentorshipLimitException : BusinessRuleException
    {
        public StudentMentorshipLimitException() :
            base("El/la estudiante ya tiene el máximo de mentorías asignadas en esta área.")
        {
        }
    }
}