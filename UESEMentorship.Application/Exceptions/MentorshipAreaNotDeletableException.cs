﻿namespace UESEMentorship.Application.Exceptions
{
    public class MentorshipAreaNotDeletableException : BusinessRuleException
    {
        public MentorshipAreaNotDeletableException() :
            base("Esta área de mentoría no puede ser eliminada porque tiene mentorías asociadas.")
        {
        }
    }
}