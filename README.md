# Examen 1 - Ingeniería de Software

#### Erik Kühlmann Salazar - B84175

## Instrucciones de ejecución

- Abrir la el archivo .sln en la raíz del repositorio.
- Ejecutar el proyecto UESEMentorship.Data para crear la base de datos.
- Ejecutar el proyecto UESEMentorship para poder utilizar la aplicación.

Requiere como mínimo la versión 5.0 del SDK de .NET, disponible [aquí](https://dotnet.microsoft.com/download/dotnet/5.0).
> Note que esta versión no viene incluida con Visual Studio todavía.

Si tiene problemas para ejecutar el proyecto desde Visual Studio, puede utilizar el siguiente comando luego de haber ejecutado el proyecto de datos:
```bash
dotnet run -p UESEMentorship
```